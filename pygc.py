import csv, urllib2, simplejson
from sys import argv

""" 
Usage: python pygc.py input.csv output.csv 
The input CSV file should be pipe (|) delimited with an 'ID' and an 'address' column. Any other columns are ignored.
Example File:
ID|name|address
1|Google|1600 amphitheatre parkway mountain view, ca
2|Microsoft|One Microsoft Way Redmond, WA 98052-6399
3|Yahoo|701 First Avenue Sunnyvale, CA 94089
"""

script, argCSV, argKML = argv

def write_header():
    """ Can be called to write a header in the output file """
    output_file.write('head')


def write_footer():
    """ Can can be called to write a header in the output file """
    output_file.write('foot')


def error_handler(error):
    """Accepts a string of test and writes it to the lookup_error.txt file.""" 
    lookup_err.write(error + '\n')

def write_content(locationDict, output_type):
   
    if output_type == 'SQL':
        output_file.write("\
                            UPDATE dbo.TableName SET\n\
                                [FullAddress] = '%s'\n\
                               ,[Lat] =  '%s'\n\
                               ,[Lng] = '%s'\n\
                            Where [ID] = '%s'\n\n"\
                          % (locationDict['formatted_addr'].encode('utf-8'), locationDict['lat'], locationDict['lng'], locationDict['ID']))
    else:
        output_file.write("%s|%s|%s|%s" % ((addr['ID'], addr['formatted_address'].encode('utf-8'), addr['lat'], addr['lng'])))

def geocode(addr):
    """Accepts the address, city, and country specified in the CSV file and finds their latitude and longitude using Googles Geocoder V3"""
    urllib_status = None
    url = "http://maps.googleapis.com/maps/api/geocode/json?address=" + addr.replace(' ', '+') + "&sensor=false"

    try:
        json = simplejson.load(urllib2.urlopen(url))
        status = json['status']
    except (IOError, UnboundLocalError):
        urllib_status = "URLLIB_SOCKET_ERROR. Probably a connection issue."
        
    if urllib_status:
        return "0", "0", urllib_status, None
    elif status != "OK":
        return "0", "0", status, None
    else:
        formatted_addr = json['results'][0]['formatted_address']
        lat = json['results'][0]['geometry']['location']['lat']
        lng = json['results'][0]['geometry']['location']['lng']
        
        return lat, lng, status, formatted_addr

def CSVtoDict(csvlist):
    """Accepts the CSV file, calls the geocode method to find lat, lng and then puts them into a dictionary.
    It also makes a list of all the addresses that could not be looked up. This list is later given to the geocode_zero_results method"""

    addrDict = csv.DictReader(open(argCSV, 'r'), delimiter='|')

    for addr in addrDict:
        lat, lng, status, formatted_addr = geocode(addr['address'])
        addr['lat'] = lat
        addr['lng'] = lng
        addr['status'] = status
        addr['formatted_addr'] = formatted_addr
        

        if DEBUG_MODE:
            print "\n"
            print "Geocoded Address: %s" % addr['formatted_addr']
            print "Geocode Status: %s" % addr['status']
            print "Latitude: %s" % addr['lat']
            print "Longitude: %s" % addr['lng']

        if addr['status'] != 'OK':
            error_handler("%s|%s|%s" % (addr['ID'], addr['address'], addr['status']))
        else:
            write_content(addr, OUTPUT_TYPE )

DEBUG_MODE = True
OUTPUT_TYPE = 'SQL'

output_file = open(argKML, 'w')
lookup_err = open('lookup_error.txt', 'w')
lookup_err.write("Goecode lookup returned no results for the feature(s) below. These entries have not been written to the output file.\n\nFormat: ID|Address|Problem Type\n")

#output_header()
CSVtoDict(argCSV)
#output_footer()
