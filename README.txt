Summary:
A simple geocoder that takes in a list of addresses in csv format and outputs a new list with latitude and longitude.

Package requirements: 
simplejson, it's been tested with version 2.1.1.
It can be downloaded here: http://pypi.python.org/pypi/simplejson/2.1.1

(Other imported packages should exist with your python install.)

Usage: 
$ python pygc.py input.csv output.csv

The input CSV file should be pipe (|) delimited with an 'ID' and an 'address' column. Any other columns are ignored.
Example File:
	ID|name|address
	1|Google|1600 amphitheatre parkway mountain view, ca
	2|Microsoft|One Microsoft Way Redmond, WA 98052-6399
	...
